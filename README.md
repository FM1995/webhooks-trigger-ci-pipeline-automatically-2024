# Webhooks - Trigger Pipeline Jobs automatically

### Pre-Requisites
Docker

Jenkins

Jenkins Plugin 'Maven'

Git

GitLab

#### Project Outline

When we setup our pipeline, we have to manually build the Job, but what we can do to increase efficiency is automatically trigger the build using webhook API’s
So what we can do is utilise gitlab and Jenkins and for gitlab to notify Jenkins that a recent change has been made and trigger a build
A good case for this is when working on development and staging environments, but having manual intervention is key, especially for production environments
We can also schedule a pipeline to run whether it be every hour, every day at 10 PM or once a week.
In this project we will configure automatic triggering of our pipeline

We will be utilising two of the below project

Link:https://gitlab.com/FM1995/java-maven-app-2024

#### Getting started

Lets begin

We will first have to configure the gitlab plugin in Jenkins

Manage Jenkins -> Plugins -> Gitlab

![Image 1](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image1.png)


And can see it is available

![Image 2](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image2.png)

If we go Manage Jenkins -> System this is where the Gitlab connection config is

Can configure the below without the API token

![Image 3](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image3.png)

In order for Gitlab to talk to Jenkins we can generate the access token in Gitlab
By going to preferences -> Access token
Can create the token called ‘jenkins’ and give Gitlab ‘api’ permissions within Jenkins and now ready to generate


![Image 4](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image4.png)

And can see it has been generated

![Image 5](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image5.png)

And add it

![Image 6](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image6.png)

And can see the a new config called gitlab connection

![Image 7](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image7.png)

Second part is navigating to Gitlab -> Settings -> Integrations 
And the enable ‘Push’ so that if someone pushes there changes to the repository it will automatically get built out
Next we will configure the connection to Jenkins and we input the Jenkins URL
Input the project name and the Jenkins credentials


![Image 8](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image8.png)

We also need another plugin since were are testing for multi-branch pipeline
We need to install multibranch scan webhook trigger

![Image 9](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image9.png)

Can then go to the branch configuration
Lets navigate to our pipeline configuration with multi branch
Navigate to ‘Scan Multibranch Pipeline Triggers’ and select Scan by webhook and insert the token

![Image 10](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image10.png)

Can then navigate back to gitlab
Using the below format
JENKINS_URL/multibranch-webhook-trigger/invoke?token=[Trigger token]
Can then implement the in the webhook section of the repo with the token identifier ‘gitlabtoken’

![Image 11](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image11.png)

The below is the path where Jenkins will accept requests from gitlab
http://178.128.172.188:8080/multibranch-webhook-trigger/invoke?token=[gitlabtoken]
And then save the below config

![Image 12](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image12.png)

Lets make a change to the repo

![Image 13](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image13.png)

Commit it in the git repo

![Image 14](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image14.png)

Can see it automatically got generated with the build

![Image 15](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Image15.png)

Can see it automatically gets generated without manually building

![Watch Video](https://gitlab.com/FM1995/webhooks-trigger-ci-pipeline-automatically-2024/-/raw/main/Images/Recording%202024-01-27%20104548.mp4)


